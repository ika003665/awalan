<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'About';

?>
<style>
    @import url('https://fonts.googleapis.com/css?family=Patua+One');
    .head {
        color: #3c3c3c;
        background-color: #7f9693;
        height: 50px;
        width: 150px;
        text-align: center;
        border-radius: 20px 20px 20px 20px;
        position: relative;
        top: -10px;
        font-family: "Patua One";
        font-size: 20px;
        margin: 1px;
        padding: 5px 10px 5px 10px;
    }
    .head:hover {
        color: #7f9693;
        background-color: #3c3c3c;
    }
</style>
<div class="site-about">
    <a class="head" href="/basic/web/">About</a>
  
    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
