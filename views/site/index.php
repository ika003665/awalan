<style>
    @import url('https://fonts.googleapis.com/css?family=Abril+Fatface');
    @import url('https://fonts.googleapis.com/css?family=Unlock');
    @import url('https://fonts.googleapis.com/css?family=Stint+Ultra+Expanded');
    @import url('https://fonts.googleapis.com/css?family=Paytone+One');
    .site-index{
        padding: 10px 10px;
        text-align: center;
    }
    .fab.fa-shopify{
        background: #6c7f81; 
        border-radius: 20px 20px 20px 20px; 
        color: #FFFFFF; 
        font-size: 50px;
        padding: 20px 20px;
    }
    .deskripsi{
        font-family: "Abril Fatface";
        text-align: center;
        text-shadow: black 1px 1px 1px;
        font-size: 20px;
        padding-top: 20px;
        padding-bottom: 20px;
    }
    .shop {
        background-color: #aad6d9;
        padding-bottom: 5px;
        padding-top: 20px;
        border-radius: 20px 20px 20px 20px;
    }
    .fas.fa-film{
        background: #aad6d9; 
        border-radius: 20px 20px 20px 20px; 
        color: #FFFFFF; 
        font-size: 50px;
        padding: 20px 20px;
    }
    .movie {
        background-color: #83a0a3;
        padding-bottom: 5px;
        padding-top: 20px;
        border-radius: 20px 20px 20px 20px;
    }
    .fas.fa-cart-plus {
        background: #bec8c8; 
        border-radius: 20px 20px 20px 20px; 
        color: #FFFFFF; 
        font-size: 50px;
        padding: 20px 20px;
    }
    .keranjang {
        background-color: #6c7f81;
        padding-bottom: 5px;
        padding-top: 20px;
        border-radius: 20px 20px 20px 20px;
    }
    .btn.btn-outline-secondary{
        font-family: "Paytone One";
        color: #aad6d9;
        background-color: #83a0a3;
        border-radius: 10px 10px 10px 10px;
    }
    .btn.btn-outline-secondary:hover{
        font-family: "Paytone One";
        color: black;
        background-color: #6c7f81;
        border-radius: 10px 10px 10px 10px;
    }
    .btn.btn-outline-secondary1{
        font-family: "Paytone One";
        color: #aad6d9;
        background-color: #6c7f81;
        border-radius: 10px 10px 10px 10px;
    }
    .btn.btn-outline-secondary1:hover{
        font-family: "Paytone One";
        color: black;
        background-color: #aad6d9;
        border-radius: 10px 10px 10px 10px;
    }
    .btn.btn-outline-secondary2{
        font-family: "Paytone One";
        color: #136161;
        background-color: #aad6d9;
        border-radius: 10px 10px 10px 10px;
    }
    .btn.btn-outline-secondary2:hover{
        font-family: "Paytone One";
        color: black;
        background-color: #bec8c8;
        border-radius: 10px 10px 10px 10px;
    }
    .container1 {
        position: relative;
        width: 100%;
        color: red;
    }
    .judul1{
        position: relative;
        right: -1%;
        top: 5px;
    }
    .judul2{
        position: relative;
        right: -20%;
        top: -51px;
    }
    .judul3{
        position: relative;
        right: -40%;
        top: -107px;
    }
    .menulain{
        position: relative;
        color: black;
        background-color: #aad6d9;
        border: #aad6d9 solid 3px;
        width: 100px;
        left: 20px;
    }
    .menulain:hover{
        color: #6c7f81;
        background-color: #136161;
    }
</style>
<?php

/** @var yii\web\View $this */

$this->title = 'Halaman Utama';
?>
<div class="site-index">
        <div class="row">
            <div class="col-lg-4">
                <div class="shop">
                <i class='fab fa-shopify'></i>
                <h2 class="deskripsi" style="color:#6c7f81">Toko Perlengkapan MHS</h2>
                <p><a class="btn btn-outline-secondary" href="#">TOKO &raquo;</a></p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="movie">
                <i class='fas fa-film'></i>
                <h2 class="deskripsi" style="color:#aad6d9">Movie MHS</h2>
                <p><a class="btn btn-outline-secondary1" href="#">MOVIE &raquo;</a></p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="keranjang">
                <i class='fas fa-cart-plus'></i>
                <h2 class="deskripsi" style="color:#bec8c8">Keranjang MHS</h2>
                <p><a class="btn btn-outline-secondary2" href="#">KERANJANG &raquo;</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<p class="menulain">Menu Lainnya</p>
<div class="container1">
    <div class="judul1">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" class='btn btn-default' aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Judul1</a>
        <ul aria-labelledby="dropdownSubMenu1" style="background-color: #aad6d9;border-radius:5px 5px 5px 5px;" class="dropdown-menu border-100 shadow">
            <li><a href="#" class="btn btn-default">Some action </a></li>

                <!-- Level two dropdown-->
             
                <!-- End Level two -->
        </ul>
    </div>
    <div class="judul2">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" class='btn btn-default' aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Judul2</a>
        <ul aria-labelledby="dropdownSubMenu1" style="background-color: #aad6d9;border-radius:5px 5px 5px 5px;" class="dropdown-menu border-100 shadow">
            <li><a href="#" class="btn btn-default">Some action </a></li>

                <!-- Level two dropdown-->
             
                <!-- End Level two -->
        </ul>
    </div>
    <div class="judul3">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" class='btn btn-default' aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Judul3</a>
        <ul aria-labelledby="dropdownSubMenu1" style="background-color: #aad6d9;border-radius:5px 5px 5px 5px;" class="dropdown-menu border-100 shadow">
            <li><a href="#" class="btn btn-default">Some action </a></li>

                <!-- Level two dropdown-->
             
                <!-- End Level two -->
        </ul>
    </div>
</div>

