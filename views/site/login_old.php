<style>
    .container {
        background-color: #b7d1d2;
        padding: 50px;
        border-radius: 20px 20px 20px 20px;
    }
    .btn.btn-primary {
        background-color: #00aebb;
    }
    .btn.btn-primary:hover {
        background-color: #429196;
    }
    .btn.btn-success {
        background-color: #00ff9e;
    }
    .btn.btn-success:hover {
        background-color: #25986c;
    }
    
</style>
<div class="container">
<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\LoginForm $model */

use PhpParser\Node\Stmt\Label;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\helpers\BaseArrayHelper;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Masukkan Username Dan Password Dengan Benar</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}",
            'labelOptions' => ['style' => 'width:100px','class' => ''],
            'inputOptions' => ['class' => 'col-lg-3 form-control'],
            'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['style'=> 'width:200px','class'=>'text-left','autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput(['style'=> 'width:200px','class'=>'text-left']) ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"offset-lg-1 col-lg-3 custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="offset-lg-1 col-lg-11">
                <?= Html::a('Create', ['prodi-login/create'], ['class' => 'btn btn-success']) ?>
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

    <!--<div class="offset-lg-1" style="color:#00008B;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
        To modify the username/password, please check out the code <code>app\models\User::$users</code>.
    </div>-->
</div>
</div>
