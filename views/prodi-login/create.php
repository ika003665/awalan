<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ProdiLogin $model */

$this->title = 'Create Toko Khusus Mahasiswa';

?>
<div class="prodi-login-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
