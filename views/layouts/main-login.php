<style>
    @import url('https://fonts.googleapis.com/css?family=Henny+Penny');
    .logo {
    font-family: "Unlock";
    text-align: center;
    font-size: 35px;
    color: #738d8f;
    text-shadow: #2c6e71 2px 2px 2px;
    text-decoration: underline;
    }
    .image {
        width: 50px;
        height: 50px;
    }
</style>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

\hail812\adminlte3\assets\AdminLteAsset::register($this);
$this->registerCssFile('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700');
$this->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
\hail812\adminlte3\assets\PluginAsset::register($this)->add(['fontawesome', 'icheck-bootstrap']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="hold-transition login-page">
<?php  $this->beginBody() ?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?=Yii::$app->homeUrl?>"><b class="logo"> <p></p><img class="image" src="https://media.istockphoto.com/id/1266252967/id/vektor/desain-logo-toko-online.jpg?s=612x612&w=0&k=20&c=AvyUXaL5QTBUMoSlYXQPs-UwZwGcZEfEp_vsikv0P98=" alt=""><p></p> Toko Khusus Mahasiswa</b></a>
    </div>
    <!-- /.login-logo -->

    <?= $content ?>
</div>
<!-- /.login-box -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>