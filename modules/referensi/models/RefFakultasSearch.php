<?php

namespace app\modules\referensi\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\referensi\models\RefFakultas;

/**
 * RefFakultasSearch represents the model behind the search form of `app\modules\referensi\models\RefFakultas`.
 */
class RefFakultasSearch extends RefFakultas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fak_id'], 'integer'],
            [['fak_nama', 'fak_create_date', 'fak_create_by', 'fak_update_date', 'fak_update_by', 'fak_delete_date', 'fak_delete_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefFakultas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fak_id' => $this->fak_id,
            'fak_create_date' => $this->fak_create_date,
            'fak_update_date' => $this->fak_update_date,
            'fak_delete_date' => $this->fak_delete_date,
        ]);

        $query->andFilterWhere(['like', 'fak_nama', $this->fak_nama])
            ->andFilterWhere(['like', 'fak_create_by', $this->fak_create_by])
            ->andFilterWhere(['like', 'fak_update_by', $this->fak_update_by])
            ->andFilterWhere(['like', 'fak_delete_by', $this->fak_delete_by]);

        return $dataProvider;
    }
}
