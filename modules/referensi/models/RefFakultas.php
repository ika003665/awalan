<?php

namespace app\modules\referensi\models;

use Yii;

/**
 * This is the model class for table "ref_fakultas".
 *
 * @property int $fak_id
 * @property string $fak_nama
 * @property string $fak_create_date
 * @property string $fak_create_by
 * @property string $fak_update_date
 * @property string $fak_update_by
 * @property string $fak_delete_date
 * @property string $fak_delete_by
 *
 * @property Prodi[] $prodis
 */
class RefFakultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ref_fakultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fak_nama', 'fak_create_date', 'fak_create_by', 'fak_update_date', 'fak_update_by', 'fak_delete_date', 'fak_delete_by'], 'required'],
            [['fak_create_date', 'fak_update_date', 'fak_delete_date'], 'safe'],
            [['fak_nama'], 'string', 'max' => 150],
            [['fak_create_by', 'fak_update_by', 'fak_delete_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fak_id' => 'Fak ID',
            'fak_nama' => 'Fak Nama',
            'fak_create_date' => 'Fak Create Date',
            'fak_create_by' => 'Fak Create By',
            'fak_update_date' => 'Fak Update Date',
            'fak_update_by' => 'Fak Update By',
            'fak_delete_date' => 'Fak Delete Date',
            'fak_delete_by' => 'Fak Delete By',
        ];
    }

    /**
     * Gets query for [[Prodis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdis()
    {
        return $this->hasMany(Prodi::class, ['fak_id' => 'fak_id']);
    }
}
