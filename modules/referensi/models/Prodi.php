<?php

namespace app\modules\referensi\models;

use Yii;

/**
 * This is the model class for table "prodi".
 *
 * @property int $prodi_id
 * @property string $prodi_kode
 * @property string $prodi_nama
 * @property string $prodi_jenjang
 * @property int $fak_id
 * @property string $prodi_create_date
 * @property string $prodi_create_by
 * @property string $prodi_update_date
 * @property string $prodi_update_by
 * @property string $prodi_delete_date
 * @property string $prodi_delete_by
 *
 * @property RefFakultas $fak
 */
class Prodi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_kode', 'prodi_nama', 'prodi_jenjang', 'fak_id', 'prodi_create_date', 'prodi_create_by', 'prodi_update_date', 'prodi_update_by', 'prodi_delete_date', 'prodi_delete_by'], 'required'],
            [['fak_id'], 'integer'],
            [['prodi_create_date', 'prodi_update_date'], 'safe'],
            [['prodi_kode'], 'string', 'max' => 6],
            [['prodi_nama'], 'string', 'max' => 150],
            [['prodi_jenjang'], 'string', 'max' => 3],
            [['prodi_create_by', 'prodi_update_by', 'prodi_delete_date', 'prodi_delete_by'], 'string', 'max' => 100],
            [['fak_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefFakultas::class, 'targetAttribute' => ['fak_id' => 'fak_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prodi_id' => 'ID',
            'prodi_kode' => 'Kode',
            'prodi_nama' => 'Nama',
            'prodi_jenjang' => 'Jenjang',
            'fak_id' => 'Fak ID' ,
            'prodi_create_date' => 'Create Date',
            'prodi_create_by' => 'Create By',
            'prodi_update_date' => 'Update Date',
            'prodi_update_by' => 'Update By',
            'prodi_delete_date' => 'Delete Date',
            'prodi_delete_by' => 'Delete By',
        ];
    }

    /**
     * Gets query for [[Fak]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFak()
    {
        return $this->hasOne(RefFakultas::class, ['fak_id' => 'fak_id']);
    }
}
