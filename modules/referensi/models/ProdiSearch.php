<?php

namespace app\modules\referensi\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\referensi\models\Prodi;

/**
 * ProdiSearch represents the model behind the search form of `app\modules\referensi\models\Prodi`.
 */
class ProdiSearch extends Prodi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_id', 'fak_id'], 'integer'],
            [['prodi_kode', 'prodi_nama', 'prodi_jenjang', 'prodi_create_date', 'prodi_create_by', 'prodi_update_date', 'prodi_update_by', 'prodi_delete_date', 'prodi_delete_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prodi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prodi_id' => $this->prodi_id,
            'fak_id' => $this->fak_id,
            'prodi_create_date' => $this->prodi_create_date,
            'prodi_update_date' => $this->prodi_update_date,
        ]);

        $query->andFilterWhere(['like', 'prodi_kode', $this->prodi_kode])
            ->andFilterWhere(['like', 'prodi_nama', $this->prodi_nama])
            ->andFilterWhere(['like', 'prodi_jenjang', $this->prodi_jenjang])
            ->andFilterWhere(['like', 'prodi_create_by', $this->prodi_create_by])
            ->andFilterWhere(['like', 'prodi_update_by', $this->prodi_update_by])
            ->andFilterWhere(['like', 'prodi_delete_date', $this->prodi_delete_date])
            ->andFilterWhere(['like', 'prodi_delete_by', $this->prodi_delete_by]);

        return $dataProvider;
    }
}
