<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\Prodi $model */

$this->title = $model->prodi_id;
$this->params['breadcrumbs'][] = ['label' => 'Prodi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prodi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'prodi_id' => $model->prodi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'prodi_id' => $model->prodi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prodi_id',
            'prodi_kode',
            'prodi_nama',
            'prodi_jenjang',
            'fak_id',
            'prodi_create_date',
            'prodi_create_by',
            'prodi_update_date',
            'prodi_update_by',
            'prodi_delete_date',
            'prodi_delete_by',
        ],
    ]) ?>

</div>
