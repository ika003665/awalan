<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\Prodi $model */

$this->title = 'Update Prodi: ' . $model->prodi_id;
$this->params['breadcrumbs'][] = ['label' => 'Prodi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->prodi_id, 'url' => ['view', 'prodi_id' => $model->prodi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prodi-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
