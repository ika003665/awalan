<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\Prodi $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="prodi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prodi_kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_jenjang')->dropDownList([ 'D3' => 'D3','S1' => 'S1','S2' => 'S2','S3' => 'S3' ], ['prompt' => 'Silahkan Pilih Jenjang Anda']) ?>

    <?= $form->field($model, 'fak_id')->textInput() ?>

    <?= $form->field($model, 'prodi_create_date')->textInput() ?>

    <?= $form->field($model, 'prodi_create_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_update_date')->textInput() ?>

    <?= $form->field($model, 'prodi_update_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_delete_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_delete_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
