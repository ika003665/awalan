<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\ProdiSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="prodi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'prodi_id') ?>

    <?= $form->field($model, 'prodi_kode') ?>

    <?= $form->field($model, 'prodi_nama') ?>

    <?= $form->field($model, 'prodi_jenjang') ?>

    <?= $form->field($model, 'fak_id') ?>

    <?php // echo $form->field($model, 'prodi_create_date') ?>

    <?php // echo $form->field($model, 'prodi_create_by') ?>

    <?php // echo $form->field($model, 'prodi_update_date') ?>

    <?php // echo $form->field($model, 'prodi_update_by') ?>

    <?php // echo $form->field($model, 'prodi_delete_date') ?>

    <?php // echo $form->field($model, 'prodi_delete_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
