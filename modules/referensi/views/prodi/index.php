<?php

use app\modules\referensi\models\Prodi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\ProdiSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Prodi';

?>
<style>
    @import url('https://fonts.googleapis.com/css?family=Patua+One');
    .head {
        color: #3c3c3c;
        background-color: #7f9693;
        height: 50px;
        width: 150px;
        text-align: center;
        border-radius: 20px 20px 20px 20px;
        position: relative;
        top: -10px;
        font-family: "Patua One";
        font-size: 30px;
        margin: 1px;
        padding: 5px 10px 5px 10px;
    }
    .head:hover {
        color: #7f9693;
        background-color: #3c3c3c;
    }
</style>
<div class="prodi-index">

    <a class="head" href="/basic/web/">User Register</a>

    <p></p>
    <p>
        <?= Html::a('Create Prodi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'  => [
            ['class' => 'yii\grid\SerialColumn'],

            /*[
                'label' => "ID",
                'headerOptions' => ['style'=> 'width:20px','class'=>''],
                'value' => function ($model) {
                    return $model->prodi_id;
                }
            ],*/
            [
                'header' => "Kode",
                'headerOptions' => ['style'=> 'width:30px','class'=>''],
                'value' => function ($model) {
                    return $model->prodi_kode;
                }
            ],
            [
                'label' => "Nama Prodi",
                'headerOptions' => ['style'=> 'width:600px','class'=>'text-center'],
                'value' => function ($model) {
                    return $model->prodi_nama;
                }
            ],
            [
                'label' => "Nama Jenjang",
                'headerOptions' => ['style'=> 'width:150px','class'=>'text-center'],
                'value' => function ($model) {
                    return $model->prodi_jenjang;
                }
            ],
            [
                'label' => "Fakultas",
                'headerOptions' => ['style'=> 'width:200px','class'=>'text-center'],
                'value' => function ($model) {
                    return $model->fak->fak_nama;
                }
            ],
            //'fak_id',
            //'prodi_create_date',
            //'prodi_create_by',
            //'prodi_update_date',
            //'prodi_update_by',
            //'prodi_delete_date',
            //'prodi_delete_by',
            [
                'header' => 'Aksi',
                'headerOptions' => ['style'=> 'width:80px','class'=>'text-center'],
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Prodi $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'prodi_id' => $model->prodi_id]);
                 }
            ],
        ],
    ]); ?>


</div>
