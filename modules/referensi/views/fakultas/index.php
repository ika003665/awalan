<?php

use app\modules\referensi\models\RefFakultas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\RefFakultasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ref Fakultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-fakultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ref Fakultas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*[
                'header' => "ID",
                'headerOptions' => ['style'=> 'width:30px','class'=>''],
                'value' => function ($model) {
                    return $model->fak_id;
                }
            ],*/
            [
                'header' => "Nama Fakultas",
                'headerOptions' => ['style'=> 'width:400px','class'=>'text-center'],
                'value' => function ($model) {
                    return $model->fak_nama;
                }
            ],
            [
                'header' => "Tanggal Buat",
                'headerOptions' => ['style'=> 'width:200px','class'=>''],
                'value' => function ($model) {
                    return $model->fak_create_date;
                }
            ],
            [
                'header' => "Dibuat Oleh",
                'headerOptions' => ['style'=> 'width:200px','class'=>''],
                'value' => function ($model) {
                    return $model->fak_create_by;
                }
            ],
            [
                'header' => "Tanggal Update",
                'headerOptions' => ['style'=> 'width:200px','class'=>''],
                'value' => function ($model) {
                    return $model->fak_update_date;
                }
            ],
            //'fak_create_date',
            //'fak_create_by',
            //'fak_update_date',
            //'fak_update_by',
            //'fak_delete_date',
            //'fak_delete_by',
            [
                'header' => 'Aksi',
                'headerOptions' => ['style'=> 'width:80px','class'=>'text-center'],
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, RefFakultas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'fak_id' => $model->fak_id]);
                 }
            ],
        ],
    ]); ?>


</div>
