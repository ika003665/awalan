<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\RefFakultas $model */

$this->title = 'Update Ref Fakultas: ' . $model->fak_id;
$this->params['breadcrumbs'][] = ['label' => 'Ref Fakultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fak_id, 'url' => ['view', 'fak_id' => $model->fak_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-fakultas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
