<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\RefFakultas $model */

$this->title = $model->fak_id;
$this->params['breadcrumbs'][] = ['label' => 'Ref Fakultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ref-fakultas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'fak_id' => $model->fak_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'fak_id' => $model->fak_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fak_id',
            'fak_nama',
            'fak_create_date',
            'fak_create_by',
            'fak_update_date',
            'fak_update_by',
            'fak_delete_date',
            'fak_delete_by',
        ],
    ]) ?>

</div>
