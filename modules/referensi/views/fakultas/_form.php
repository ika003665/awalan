<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\RefFakultas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ref-fakultas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fak_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fak_create_date')->textInput()?>

    <?= $form->field($model, 'fak_create_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fak_update_date')->textInput() ?>

    <?= $form->field($model, 'fak_update_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fak_delete_date')->textInput() ?>

    <?= $form->field($model, 'fak_delete_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
