<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\referensi\models\RefFakultasSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ref-fakultas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fak_id') ?>

    <?= $form->field($model, 'fak_nama') ?>

    <?= $form->field($model, 'fak_create_date') ?>

    <?= $form->field($model, 'fak_create_by') ?>

    <?= $form->field($model, 'fak_update_date') ?>

    <?php // echo $form->field($model, 'fak_update_by') ?>

    <?php // echo $form->field($model, 'fak_delete_date') ?>

    <?php // echo $form->field($model, 'fak_delete_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
