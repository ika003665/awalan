<?php

namespace app\modules\referensi\controllers;

use app\modules\referensi\models\RefFakultas;
use app\modules\referensi\models\RefFakultasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FakultasController implements the CRUD actions for RefFakultas model.
 */
class FakultasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all RefFakultas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RefFakultasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefFakultas model.
     * @param int $fak_id Fak ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($fak_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($fak_id),
        ]);
    }

    /**
     * Creates a new RefFakultas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new RefFakultas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'fak_id' => $model->fak_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RefFakultas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $fak_id Fak ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($fak_id)
    {
        $model = $this->findModel($fak_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'fak_id' => $model->fak_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RefFakultas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $fak_id Fak ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($fak_id)
    {
        $this->findModel($fak_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefFakultas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $fak_id Fak ID
     * @return RefFakultas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($fak_id)
    {
        if (($model = RefFakultas::findOne(['fak_id' => $fak_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
